.. _tutorial:

Tutorial
========

.. sidebar:: Requirements

    The tutorial assumes a basic proficiency in python.
    The first chapter will take about 20 to 30 minutes to complete.

This tutorial demonstrates how to implement a basic computational workflow with signac.
The signac framework assists us in conducting a computational investigation by managing the data space for us.
This means that we do not need to worry about how to organize our data on disk and how to keep track of meta data.

**As a beginner, make sure to complete the first chapter.
The second chapter deals with more advanced topics that may not be of interest to everyone.**

.. tip::

    This tutorial and other examples are available as `interactive jupyter notebooks <signac-examples_>`_!
    The notebooks are also hosted `online <online tutorial_>`_ as a free service by courtesy of `<mybinder_>`_, however online notebooks are not always available.

.. _`signac-examples`: https://bitbucket.org/glotzer/signac-examples
.. _`online tutorial`: http://www.mybinder.org/repo/csadorf/signac-examples
.. _`mybinder`: http://http://www.mybinder.org

Contents:

.. toctree::
   :maxdepth: 1

   signac_101_Getting_Started
   signac_102_Exploring_Data
   signac_103_A_Basic_Workflow
   signac_104_Modifying_the_Data_Space
   signac_105_Command_Line_Interface
   signac_201_Advanced_Indexing
   signac_202_Integration_with_pandas
   signac_203_Database_Integration
   signac_204_External_Tools
